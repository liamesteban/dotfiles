#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Aliases
alias dmesg="dmesg --human --color=always"
# alias rm='echo "Use \\\rm or trash (trash-cli)."; false'

alias ls="exa -lba"
alias free='free -h'

# Connect to running emacs server / daemon
# emacs_daemon_socket=$(lsof -c emacs 2>/dev/null | grep server | tail -1 | awk '{print $(NF-1)}')
emacs_daemon_socket=$(lsof -c Emacs | grep server | tr -s " " | cut -d' ' -f8)
# Add "-a ''" if $emacs_daemon_socket is empty
alias emacs="emacsclient -c -s $emacs_daemon_socket"

# Notify on long-running commands
# install undistract-me
source /usr/share/undistract-me/long-running.bash
notify_when_long_running_commands_finish_install

eval "$(fasd --init auto)"

# Get environment variables
source ~/.zshenv

if [[ -n ${INSIDE_EMACS} ]]; then
    unsetopt zle
fi
