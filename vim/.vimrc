" ViMrc
" Liam Esteban

" Use za or space to fold / unfold or remove the last line to disable folding: " vim:foldmethod=marker

" Debian {{{
" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those
" settings, you should do it in this file (/etc/vim/vimrc), since debian.vim
" will be overwritten everytime an upgrade of the vim packages is performed.
" It is recommended to make changes after sourcing debian.vim since it alters
" the value of the 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

syntax on

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
if has("autocmd")
  filetype plugin indent on
endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden		" Hide buffers when they are abandoned
"if set, third click no longer allows pasting from outside vim
set mouse=a		" Enable mouse usage (all modes)
set nocompatible        " Not compatible with regular vi

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif
" }}}

" Colors {{{
" colorscheme solarized " gruvbox | dante | solarized
" set background=light " dark | light "
filetype plugin on

"set t_Co=16
"set t_Co=256                        " force vim to use 256 colors

" let g:gruvbox_italic=1

" }}}

" Plugins (Plug-vim) {{{
call plug#begin('~/.vim/plugged')
Plug 'meitham/vim-spacemacs'
Plug 'plasticboy/vim-markdown'
Plug '907th/vim-auto-save'
Plug 'pearofducks/ansible-vim'
Plug 'wakatime/vim-wakatime'
Plug 'jceb/vim-orgmode'
call plug#end()
" }}}

" For writing (Markdown) {{{

au BufRead,BufNewFile *.markdown setlocal textwidth=80
au BufRead,BufNewFile *.rst setlocal textwidth=80
" au BufRead *.markdown normal zR
" zR open all folds. zM close all
" moving between paragraphs, '{', '}' or [[, ]]

" see http://www.drbunsen.org/writing-in-vim/
" http://naperwrimo.org/wiki/index.php?title=Vim_for_Writers
" [prose with vim](http://alols.github.io/2012/11/07/writing-prose-with-vim/)
" [vim for
" writers](http://naperwrimo.org/wiki/index.php?title=Vim_for_Writers)
" [list of
" plugins](http://wynnnetherland.com/journal/reed-esau-s-growing-list-of-vim-plugins-for-writers/)
" [text triumverate](http://www.drbunsen.org/the-text-triumvirate/)
" [cheat writing in vim](http://www.drbunsen.org/writing-in-vim/)
"
" }}}

" Modifications {{{
" http://learnvimscriptthehardway.stevelosh.com/

let mapleader = "\\"

" Tabs and buffers
" http://joshldavis.com/2014/04/05/vim-tab-madness-buffers-vs-tabs/
" http://stackoverflow.com/questions/26708822/why-do-vim-experts-prefer-buffers-over-tabs

" Operator pending mappings
" (http://learnvimscriptthehardway.stevelosh.com/chapters/15.html)
" Editing
:nnoremap <leader>ev :vsplit $MYVIMRC<cr>
:nnoremap <leader>sv :source $MYVIMRC<cr>

" :help autocmd-groups

" Plugins {{{
" Autosave
let g:auto_save = 1  " enable AutoSave on Vim startup
let g:auto_save_silent = 1  " do not display the auto-save notification
let g:auto_save_events = ["InsertLeave", "TextChanged"] " Save on leaving insert mode and after any normal mode operation

"gx to open link, following line to set browser
let g:netrw_browsex_viewer= "google-chrome-stable"
set title
" http://learnvimscriptthehardway.stevelosh.com/chapters/12.html"{{{"}}}
autocmd BufWritePost .Xresources !xrdb <afile>
cmap w!! w !sudo tee % >/dev/null

" map in insert mode
:inoremap jk <Esc>
"copy to clipboard :%w !xclip -i -sel c
" tabs: http://tedlogan.com/techblog3.html
" visual spaces per tab, tabs are spaces
set softtabstop=4 shiftwidth=4 expandtab
" set hlsearch " highlight search
set foldlevelstart=10   " open most folds by default
" space open/closes folds
" nnoremap: do what za does by default, even if za is mapped to something
" else
" nnoremap <space> za
" move vertically by visual line
nnoremap j gj
nnoremap k gk
" Check the end of the file for a modeline
set modelines=1
" Spelling
" set spell spelllang=en_gb
" drop down menu to change misspelled word
noremap \s ea<C-X><C-S>
" }}}
" Navigating {{{
:nnoremap <Tab> :bnext<CR>
:nnoremap <S-Tab> :bprevious<CR>
" Tab used to be go to next link, change that to something else

" Fonts {{{
set guifont=Monaco\ 12
" }}}

" vim:foldmethod=marker:foldlevel=2
