# Dotfiles
Managed with [stow](https://www.gnu.org/software/stow/)

* Do not add files that are for programs, not humans

This repository should track meaningful changes to dotfiles

* Add only configuration for programs that will be used often

If necessary, use a separate branch to keep programs with dotfiles that were once useful
